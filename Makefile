prefix     ?= /usr
sysconfdir ?= /etc

all:
clean:

install:
	install -d $(DESTDIR)$(prefix)/share/kboot/
	install -m 0644 kboot-mkconfig_lib $(DESTDIR)$(prefix)/share/kboot/kboot-mkconfig_lib
	install -d $(DESTDIR)$(prefix)/sbin
	install -m 0755 kboot-mkconfig $(DESTDIR)$(prefix)/sbin/kboot-mkconfig
	install -d $(DESTDIR)$(sysconfdir)/default
	install -m 0644 default.kboot $(DESTDIR)$(sysconfdir)/default/kboot

changelog:
	git log --pretty="format:%ai  %aN  <%aE>%n%n%x09* %s%d%n" > ChangeLog
